========================
Tweecluster
========================

Tweecluster is  python library for clustering short text.



Requirement
=================

- Python 3.2


Setup project
=================

Tweecluster is open source project publically available on Bitbucket. To clone the repository, use the following command.

.. code-block:: bash

	git clone https://bitbucket.org/sparkindata-irit/tweecluster.git
	cd tweecluster
	chmod a+x bin/*




Clustering documents
=======================

To cluster tweet form statuses log file, use the folowwing command line.

.. code-block:: bash

	./bin/cluster -statuses_log datasets/status.log -suffix sample  --out output/sample



Version
===============

Tweecluster 0.0.1


Contributors
===============

The following people have contributed to this code:

- Lamjed Ben Jabeur `Lamjed.Ben-Jabeur@irit.fr <mailto:Lamjed.Ben-Jabeur@irit.fr>`_.


License
===============
This software is governed by the `CeCILL-B license <LICENSE.txt>`_ under French law and abiding by the rules of distribution of free software.  You can  use, modify and/ or redistribute the software under the terms of the CeCILL-B license as circulated by CEA, CNRS and INRIA at the following URL
`http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html <http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html>`_.