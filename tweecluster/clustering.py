import re
import unicodedata
from abc import ABCMeta, abstractmethod

import nltk
import numpy as np
from nltk.corpus import stopwords

twitter_stopwords = ['data/swearwords.txt', 'data/not-informative-english.txt', 'data/custom-stopwords.txt',
                     'data/twitter-stopwords.txt', 'data/news-twitter-accounts.txt', 'data/news-media.txt']


class Textual:
    def __init__(self, text, id=None, object=None):
        self.__text = text
        self.__id = id
        self.__object = object

    @property
    def text(self):
        return self.__text

    @property
    def id(self):
        return self.__id

    @property
    def object(self):
        return self.__object

    def __str__(self):
        return '(' + self.id + ', ' + re.sub('\s+', ' ', self.text) + ')'

    def __repr__(self):
        return self.__str__()


class IClusterer(metaclass=ABCMeta):
    def __init__(self):
        self.similarity_cache = {}

    def centroid(self, cluster):
        if len(cluster) > 0:
            return cluster[0]
        else:
            return None

    @abstractmethod
    def cluster(self, documents=None):
        pass

        new_clusters = []
        if clusters:
            centroids = []
            for cluster in clusters:
                centroids.append(self.centroid(cluster))
            centroid_clusters = self.cluster(centroids, recluster=False)
            for cluster in centroid_clusters:
                new_cluster = []
                for centroid in cluster:
                    for i in range(0, len(centroids)):
                        if centroid == centroids[i]:
                            for document in clusters[i]:
                                new_cluster.append(document)
                            break
                new_clusters.append(new_cluster)
        return new_clusters

    def add_to_similarity_cache(self, id_a=None, id_b=None, score=None):
        if id_a not in self.similarity_cache:
            self.similarity_cache[id_a] = {}
        if id_b not in self.similarity_cache:
            self.similarity_cache[id_b] = {}
        self.similarity_cache[id_a][id_b] = score
        self.similarity_cache[id_b][id_a] = score

    def get_similarity_cache(self, id_a=None, id_b=None):
        if id_a in self.similarity_cache:
            if id_b in self.similarity_cache[id_a]:
                return self.similarity_cache[id_a][id_b]
        if id_b in self.similarity_cache:
            if id_a in self.similarity_cache[id_b]:
                return self.similarity_cache[id_b][id_a]
        return None

    @staticmethod
    def print(clusters=None):
        if clusters:
            for i in range(0, len(clusters)):
                print("cluster #" + str(i + 1))
                for j in range(0, len(clusters[i])):
                    print("\t #" + str(j + 1) + "\t\t" + str(clusters[i][j]))

    def write_similarities_csv(self, path=None):
        out = open(path, 'w', encoding="UTF-8")
        for id_a in self.similarity_cache:
            for id_b in self.similarity_cache[id_a]:
                line = id_a + "\t" + id_b + "\t" + str(self.similarity_cache[id_a][id_b])
                out.write(line)
                out.flush()
        out.close()

    @staticmethod
    def write_csv(clusters=[], filename=None):
        out = open(filename, 'w', encoding="UTF-8")
        for i in range(0, len(clusters)):
            cluster = clusters[i]
            for j in range(0, len(cluster)):
                line = str(i + 1) + "\t" + str(cluster[j].id)
                out.write(line)
                out.flush()
        out.close()

    @staticmethod
    def write_html(clusters=[], filename='cluters.html', title="Clusters"):
        out = open(filename, 'w', encoding="UTF-8")
        out.write('<!DOCTYPE html>')
        out.write('<html lang="en">')
        out.write('<head>')
        out.write('<meta charset="utf-8"/>')
        out.write('<title>' + title + '</title>')
        out.write(
            '<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet"/>')
        out.write(
            '<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" rel="stylesheet"/>')
        out.write('<style>')
        out.write('#sentence-words span{display:inline-block;}')
        out.write('</style>')
        out.write('</head>')
        out.write('<body>')
        out.write(
            '<div class="container"><div class="page-header"><h1>' + title + '</h1><p class="lead">Visualizing Tweets clusters</p></div>')
        i = 0
        for i in range(0, len(clusters)):
            cluster = clusters[i]
            # clusterid = i * 100000 + i + 1
            # print(i)
            out.write(
                '<div class="row"><div class="col-md-12">')

            out.write('<div class="panel-group">')
            out.write('<div class="panel panel-default">')
            out.write('<div class="panel-heading">')
            out.write('<h4>')
            out.write('<span class="label label-info">Cluster # ' + str(i + 1) + '</span>')
            out.write('<span class="badge pull-right"">' + str(len(cluster)) + '</span>')
            out.write('<h4>')
            out.write(
                '<button type="button" class="btn btn-default  pull-right"  data-toggle="collapse" href="#collapse' + str(
                    i) + '"><span class="glyphicon glyphicon-list" aria-hidden="true"></span> View all</button>')

            out.write(cluster[0])
            out.write('</h4>')

            out.write('</div>')
            out.write('<div id="collapse' + str(i) + '" class="panel-collapse collapse">')
            out.write('<ul class="list-group">')
            for j in range(0, len(cluster)):
                out.write(
                    '<li class="list-group-item"><span class="badge pull-left"">' + str(j + 1) + "</span> " + cluster[
                        j].text + '</li>')
            out.write('</ul>')
            out.write('</div>')
            out.write('</div>')
            out.write('</div>')
            out.write('</div></div>')
        out.write('/<div>')
        out.write('<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>')
        out.write('<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>')
        out.write('<script src="script.js"></script>')
        out.write('<script src="jquery.tweet-linkify.js"></script>')
        out.write('</body></html>')
        out.close()


class NearDuplicateClusterer(IClusterer):
    def __init__(self, lang='english', stopwords_files=None):
        super().__init__()
        self.__stemmer = nltk.stem.SnowballStemmer(lang)
        self.__stopwords = stopwords.words("english")
        files = [] if not stopwords_files else stopwords_files
        if isinstance(files, list):
            for file in files:
                self.__stopwords = self.__load_file(file, self.__stopwords)

    def __load_file(self, filename, list=None):
        if not list:
            list = []
        with open(filename, "r") as ins:
            for line in ins:
                word = line.strip().lower()
                if word:
                    if word not in list:
                        list.append(word)
        return list

    def __strip_accents(self, text):
        try:
            text = unicode(text, 'utf-8')
        except NameError:
            pass
        text = unicodedata.normalize('NFD', text)
        text = text.encode('ascii', 'ignore')
        text = text.decode("utf-8")
        return str(text)

    def __strip_urls(self, text):
        return re.sub('https?\://.*/\w+', '', text)

    def __strip_ponctuation(self, text):
        return re.sub('[@#.,"\'?!:;\(\)\[\]\{\}\-_\\\/\^\$]+', ' ', text)

    def __tokenize(self, text):
        return [token for token in nltk.word_tokenize(text) if token not in self.__stopwords]

    def __stem(self, text):
        return [self.__stemmer.stem(token) for token in self.__tokenize(text)]

    def __hash(self, text):
        text = self.__strip_urls(text)
        text = self.__strip_ponctuation(text)
        text = self.__strip_accents(text)
        stems = self.__stem(text)
        stems.sort()
        text = ' '.join(stems)
        text = re.sub('\s+', ' ', text).strip().lower()
        return text

    def cluster(self, documents=None, recluster=True):
        if documents:
            hash_documents = {}
            for document in documents:
                hash = self.__hash(document.text)
                if hash not in hash_documents:
                    hash_documents[hash] = []
                hash_documents[hash].append(document)
            clusters = []
            for key in hash_documents:
                clusters.append(hash_documents[key])
                for i in range(0, len(hash_documents[key]) - 1):
                    for j in range(i + 1, len(hash_documents[key])):
                        self.add_to_similarity_cache(hash_documents[key][i].id, hash_documents[key][j].id, 1)
            clusters.sort(key=lambda x: len(x), reverse=True)
        return clusters


class SequentialClusterer(NearDuplicateClusterer):
    gamma = 0.3
    h = 0.6

    def __init__(self, gamma=None, h=None, rho=None, n=None):
        super().__init__()
        self.h = SequentialClusterer.h if not h else h
        self.gamma = SequentialClusterer.gamma if not gamma else gamma

    def __tokenize(self, text):
        return nltk.word_tokenize(text)

    def __aggregate(self, scores=None):
        if scores:
            a = np.array(scores)
            return np.percentile(a, self.h * 100)

    def __similarity(self, tokens_a=None, tokens_b=None):
        words_a = set(tokens_a) if tokens_a else set()
        words_b = set(tokens_b) if tokens_b else set()
        if len(words_b) < len(words_a):
            tmp = words_a
            words_a = words_b
            words_b = tmp

        interaction = words_a.intersection(words_b)
        norm = len(words_b) + len(words_a)
        if norm > 0:
            return (2.0 * len(interaction)) / norm
        return 0

    def cluster(self, documents=None, recluster=True):
        clusters = super().cluster(documents)
        if recluster and len(clusters) > 0:
            return self.recluster(clusters)
        else:
            clusters = []
            if documents:
                for document_a in documents:
                    tokens_a = self.__tokenize(document_a.text)
                    best_cluster_index = None
                    best_similarity = -1
                    i = 0
                    for cluster in clusters:
                        similarity_scores = []
                        for document_b in cluster:
                            similarity_score = self.get_similarity_cache(document_a.id, document_b.id)
                            if not similarity_score:
                                tokens_b = self.__tokenize(document_b.text)
                                similarity_score = self.__similarity(tokens_a, tokens_b)
                                self.add_to_similarity_cache(document_a.id, document_b.id, similarity_score)
                            similarity_scores.append(similarity_score)
                        score = self.__aggregate(similarity_scores)
                        if score > best_similarity:
                            best_cluster_index = i
                            best_similarity = score
                        i += 1
                    if best_similarity >= self.gamma:
                        clusters[best_cluster_index].append(document_a)
                    else:
                        clusters.append([document_a])
                for cluster in clusters:
                    cluster.sort(key=lambda x: x.id)
                clusters.sort(key=lambda x: len(x), reverse=True)
            return clusters
